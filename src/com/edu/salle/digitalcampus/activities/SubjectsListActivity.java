package com.edu.salle.digitalcampus.activities;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.adapter.SubjectAdapter;
import com.edu.salle.digitalcampus.dao.SubjectDAO;
import com.edu.salle.digitalcampus.objects.Subject;

public class SubjectsListActivity extends BaseActivity {
	
	private ListView myListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_subjects_list);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		associateControls();
		prepareActivity();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.subjects_list, menu);
		getActionBar().setSubtitle(R.string.subtitle_activity_subjects_list);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_subject:
			Toast.makeText(getApplicationContext(), "New Subject", Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void associateControls() {
		myListView = (ListView) findViewById(R.id.subjects_listview);
	}
    
    private void prepareActivity() {
    	List<Subject> data = loadSubjects();
    	
    	SubjectAdapter adapter = new SubjectAdapter(getApplicationContext(),R.layout.subject_list_item,data);
    	myListView.setAdapter(adapter);
	}

	private List<Subject> loadSubjects() {
		List<Subject> data = new ArrayList<Subject>();
		SubjectDAO subjectDAO = new SubjectDAO(getApplicationContext());
		
		
		if (!subjectDAO.isAnyTable(true)) Log.d("DATABASE", "No hay tablas!!");
		if (subjectDAO.isTableExists("Subject", true)) data = subjectDAO.getAllSubjects();
		
		return data;
	}

}
