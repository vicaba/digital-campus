package com.edu.salle.digitalcampus.activities;

import com.edu.salle.digitalcampus.DigitalCampusApp;

import android.app.Activity;

public class BaseActivity extends Activity {
	
	public static final String PARAM_FROM_NEXT_STEP_ASSISTANT = "FROM_NEXT_STEP_ASSISTANT";
	
	public DigitalCampusApp getApp() {
		return (DigitalCampusApp) getApplication();
	}

}
