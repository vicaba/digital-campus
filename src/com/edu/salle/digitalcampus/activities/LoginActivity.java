package com.edu.salle.digitalcampus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;

public class LoginActivity extends BaseActivity {
	
	private static final String APP_EMAIL = "administrador@salleurl.edu";
	private static final String APP_PASSWORD = "123qwe";
	
	private Button btnSend;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		associateControls();
	}

	private void associateControls() {
		
		this.btnSend = (Button) findViewById(R.id.login_btnSend);
		this.btnSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(),"Sending form...",Toast.LENGTH_SHORT).show();
				
				if (validateUser()) launchMenu();
				else {
					Toast.makeText(getApplicationContext(),"Wrong user or password",Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private boolean validateUser() {
		TextView textEmail = (TextView) findViewById(R.id.login_textEmail);
		String email = textEmail.getText().toString();
		TextView textPass = (TextView) findViewById(R.id.login_textPass);
		String pass = textPass.getText().toString();
		
		if (email.equals(APP_EMAIL) && pass.equals(APP_PASSWORD)) {
			return true;
		}
		return false;
	}
	
	private void launchMenu() {
		Toast.makeText(getApplicationContext(),"Success!!",Toast.LENGTH_SHORT).show();

		Intent i = new Intent(LoginActivity.this, MenuActivity.class);
		startActivity(i);
	}

}
