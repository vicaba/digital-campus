package com.edu.salle.digitalcampus.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.objects.Student;
import com.edu.salle.digitalcampus.util.Tuple2;

public class SubjectStudentAdapter extends ArrayAdapter<Student> {
	private Tuple2<ArrayList<Student>, ArrayList<Boolean>> data;
	private int my_layout;

	public SubjectStudentAdapter(Context context, int resource, Tuple2<ArrayList<Student>, ArrayList<Boolean>> data) {
		super(context, resource);
		this.data = data;
		this.my_layout = resource;
	}
	
	@Override
	public int getCount() {
		return data.get_1().size();
	}
	
	@Override
	public Student getItem(int position) {
		return data.get_1().get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		//1. Create view
		if (row == null){
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(my_layout, parent, false);
			row.setTag(position);
			row.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int pos = Integer.parseInt(v.getTag().toString());
					String text = "Position click "+pos;
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
				}
			});
		}
		
		//2. Capture controls of the view
		ImageView icon = (ImageView) row.findViewById(R.id.subject_student_list_icon);
		TextView name = (TextView) row.findViewById(R.id.subject_student_list_name);
		CheckBox tick = (CheckBox) row.findViewById(R.id.subject_student_list_tick);
		
		//3. Assign new values to captured control
		icon.setImageResource(data.get_1().get(position).getImage());
		name.setText(data.get_1().get(position).getName());
		name.setTextColor(Color.BLACK);
		
		tick.setChecked(data.get_2().get(position));
		tick.setTag(position);
		tick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {				
				int pos = Integer.parseInt(arg0.getTag().toString());
				String text = "Position tick "+pos;
				
				CheckBox check = (CheckBox) arg0;
				data.get_2().add(pos, check.isChecked());
				Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
			}
		});
		
		//4. Return view;
		return row;
	}

	public Tuple2<ArrayList<Student>, ArrayList<Boolean>> getData() {
		return this.data;
	}
}
