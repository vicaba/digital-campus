package com.edu.salle.digitalcampus.adapter;

import java.util.ArrayList;
import java.util.List;

import com.edu.salle.digitalcampus.util.Tuple3;

import android.R;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;


//NOT USED!!!
public class MenuAdapter extends BaseAdapter {

	private List<Tuple3<Integer, String, OnClickListener>> menuItems;
	Context context;

	public MenuAdapter(Context context) {
		super();
		this.context = context;
		this.menuItems = new ArrayList<Tuple3<Integer, String, OnClickListener>>();

		populateList();
	}

	private void populateList() {

		this.menuItems.add(new Tuple3<Integer, String, OnClickListener>(R.drawable.alert_dark_frame,
				"Manage Subjects", new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				}));
		
		this.menuItems.add(new Tuple3<Integer, String, OnClickListener>(R.drawable.alert_dark_frame,
				"Manage Students", new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				}));
		
		this.menuItems.add(new Tuple3<Integer, String, OnClickListener>(R.drawable.alert_dark_frame,
				"Tests", new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				}));
		
		this.menuItems.add(new Tuple3<Integer, String, OnClickListener>(R.drawable.alert_dark_frame,
				"Exit", new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				}));

	}

	@Override
	public int getCount() {
		return menuItems.size();
	}

	@Override
	public Object getItem(int position) {
		return menuItems.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;

		if (row == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			row = inflater.inflate(android.R.layout.simple_list_item_1, parent,
					false);
			row.setClickable(true);

		}

		/*
		 * // Get item ImageView view = this.menuItems.get(position); // Set
		 * text ImageView icon = (ImageView) row.findViewById(R.id.icon);
		 * icon.setImageResource(view.get)
		 * 
		 * TextView text1 = (TextView) row.findViewById(R.id.text1);
		 * text1.setText("hola");
		 */

		return row;
	}

}
