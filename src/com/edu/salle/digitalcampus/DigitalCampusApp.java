package com.edu.salle.digitalcampus;

import java.util.ArrayList;

import com.edu.salle.digitalcampus.objects.Student;
import com.edu.salle.digitalcampus.objects.Subject;
import com.edu.salle.digitalcampus.util.Tuple2;

import android.app.Application;

public class DigitalCampusApp extends Application {
	
	private Subject newSubject;
	private Tuple2<ArrayList<Student>, ArrayList<Boolean>> associatedStudents;
	
	public void initializeSubjectForCreation(Subject newSubject, Tuple2<ArrayList<Student>, ArrayList<Boolean>> associatedStudents) {
		this.newSubject = newSubject;
		this.associatedStudents = associatedStudents;
	}
	
	public Subject getNewSubject() {
		return newSubject;
	}
	public void setNewSubject(Subject newSubject) {
		this.newSubject = newSubject;
	}
	public Tuple2<ArrayList<Student>, ArrayList<Boolean>> getAssociatedStudents() {
		return associatedStudents;
	}
	public void setAssociatedStudents(
			Tuple2<ArrayList<Student>, ArrayList<Boolean>> associatedStudents) {
		this.associatedStudents = associatedStudents;
	}
	
	

}
