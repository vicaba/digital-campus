package com.edu.salle.digitalcampus.objects;

import java.util.ArrayList;
import java.util.List;

public class Subject {

	private String name;
	private String description;
	private int image;
	private List<String> chapters;

	public Subject(String name, String description, int image,
			ArrayList<String> chapters) {
		this.name = name;
		this.description = description;
		this.image = image;
		this.chapters = chapters;
	}
	
	public Subject() {
		this.name = null;
		this.description = null;
		this.image = -1;
		this.chapters = null;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getImage() {
		return image;
	}

	public List<String> getChapters() {
		return chapters;
	}
	
	public void setChapters(List<String> list) {
		this.chapters = list;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImage(int image) {
		this.image = image;
	}

	
}
