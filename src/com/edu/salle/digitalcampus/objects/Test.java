package com.edu.salle.digitalcampus.objects;

public class Test {

	private String date;
	private String hour;
	private String career;
	private String subjectName;
	private String classroom;
	
	public Test(String date, String hour, String career, String subjectName,
			String classroom) {
		this.date = date;
		this.hour = hour;
		this.career = career;
		this.subjectName = subjectName;
		this.classroom = classroom;
	}

	public String getDate() {
		return date;
	}

	public String getHour() {
		return hour;
	}

	public String getCareer() {
		return career;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public String getClassroom() {
		return classroom;
	}
}
